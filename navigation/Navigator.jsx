import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Home from "../screens/Home";
import Profile from "../screens/Profiles";
import Map from "../screens/Map";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Icon from "react-native-vector-icons/Ionicons";
import MaterialIcon from "react-native-vector-icons/MaterialCommunityIcons";
import CameraScreen from "../screens/CameraScreen";



const Stack = createNativeStackNavigator()
const TabStack = createBottomTabNavigator()

function ProfileNavigator () {
    return ( 
        <Stack.Navigator initialRouteName="Home">
            <Stack.Screen name="Map" component={Map} />
        </Stack.Navigator>
     );
}

function Navigator() {
    return ( 
        //Equivalent du router en react
        <TabStack.Navigator initialRouteName="Home">
            <TabStack.Screen 
                name="Home" 
                component={Home} 
                options={{
                    tabBarIcon: ({focused, color, size}) => {
                        return focused ? <Icon name='home' size={size} color={color}/> : <Icon name='home-outline' size={size} color={color}/> 
                    }
                }}
            />
            <TabStack.Screen 
                name="Profile" 
                component={ProfileNavigator} 
                options={{
                    tabBarIcon: ({focused, color, size}) => {
                        return focused ? <MaterialIcon name='account-circle' size={size} color={color}/> : <MaterialIcon name='account-circle-outline' size={size} color={color}/> 
                    }
                }}    
            />
            <TabStack.Screen 
                name="Camera" 
                component={CameraScreen} 
                options={{
                    tabBarIcon: ({focused, color, size}) => {
                        return focused ? <MaterialIcon name='account-circle' size={size} color={color}/> : <MaterialIcon name='account-circle-outline' size={size} color={color}/> 
                    }
                }}    
            />
        </TabStack.Navigator>
     );
}



export default Navigator;