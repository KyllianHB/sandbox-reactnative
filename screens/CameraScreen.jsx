import { useIsFocused } from "@react-navigation/native";
import { useEffect, useRef } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Camera, useCameraDevices } from "react-native-vision-camera";


function CameraScreen() {

    useEffect(() => {
        const requestPermission = async () => {
            const newCameraPermission = await Camera.requestCameraPermission()
            const newAudioPermission = await Camera.requestMicrophonePermission()

            console.log(newCameraPermission)
            console.log(newAudioPermission)
        }
        requestPermission()
    }, [])

    const devices = useCameraDevices()
    const device = devices.back
    const isFocused = useIsFocused()
    const camera = useRef<Camera>(null)

    if (device != null) {
        return (
            <Camera
            style={StyleSheet.absoluteFill}
            device={device}
            isActive={isFocused}
            // ref={camera}
            photo={true}
            />
        )
    } else {
        return (
            <Text>Camera</Text>
        )
    }
}

export default CameraScreen;